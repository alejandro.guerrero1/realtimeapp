#!/bin/bash

# count=$(helm list --filter 'prod' --all-namespaces | wc -l)

# if [ ${count} != 2 ]; then
#   echo "Helm release prod does not exists"
# fi

# count=$(helm list --filter 'test' --all-namespaces | wc -l)

# if [ ${count} != 2 ]; then
#   echo "Helm release test does not exists"
# fi

old=$1
replace=$2
values=$(find . -type f  -name "values.yaml")

while read -r file;
do
  sed -i "s/$old/$replace/g" $file
done <<< "${values}"