{{/*
Establishes the name of the namespace based on the Release Name.
*/}}
{{- define "getnamespace" -}}
{{- if eq $.Release.Name "blue" -}}
{{- printf "blue" -}}
{{- else if eq $.Release.Name "green" -}}
{{- printf "green" -}}
{{- end -}}
{{- end -}}