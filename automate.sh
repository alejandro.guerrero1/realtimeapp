#!/bin/bash

##### Parameters #####
# $1 -- Process: "promote" or "deploy"
# To promote projects
# Ex: ./automate.sh promote
# To deploy new project to test
# Ex: ./automate.sh deploytest

# Check if parameters are input
if [ $# -ne 1 ] ; then
  cat << EOF
Bad Arguments
To promote projects
Ex: ./automate.sh promote
To deploy new project to test
Ex: ./automate.sh deploytest
EOF
  exit 1
fi

# Variables
process=$1
prodURL="app.co"
testURL="test.app.co"


case $process in
    "promote")
        host=$(kubectl -n blue get ingress realtimeapp -o=jsonpath='{.spec.rules[?(@.host=="'${prodURL}'")].host}')

        if [ ! -z ${host} ] && [ ${host} == ${prodURL} ];
        then
            printf "\nChanging hostname value inside Ingress from namespace test...\n"
            kubectl -n green patch ingress realtimeapp --type=json -p='[{"op": "replace", "path": "/spec/rules/0/host", "value":"'${prodURL}'"}]'
            printf "Changing hostname value inside Ingress from namespace prod...\n"
            kubectl -n blue patch ingress realtimeapp --type=json -p='[{"op": "replace", "path": "/spec/rules/0/host", "value":"'${testURL}'"}]'
        else
            printf "\nChanging hostname value inside Ingress from namespace prod...\n"
            kubectl -n blue patch ingress realtimeapp --type=json -p='[{"op": "replace", "path": "/spec/rules/0/host", "value":"'${prodURL}'"}]'
            printf "Changing hostname value inside Ingress from namespace test...\n"
            kubectl -n green patch ingress realtimeapp --type=json -p='[{"op": "replace", "path": "/spec/rules/0/host", "value":"'${testURL}'"}]'
        fi
        ;;
    "deploytest")
        host=$(kubectl -n blue get ingress realtimeapp -o=jsonpath='{.spec.rules[?(@.host=="'${testURL}'")].host}')

        if [ ! -z ${host} ] && [ ${host} == ${testURL} ];
        then
            printf "Updating testing environment... \n"
            helm upgrade blue realtime-app --set app-co.ingress.hostName=${testURL}
        else
            printf "Updating testing environment... \n"
            helm upgrade green realtime-app --set app-co.ingress.hostName=${testURL}
        fi
        ;;
    *)
        cat << EOF
Bad Arguments
To promote projects
Ex: ./automate.sh promote
To deploy new project to test
Ex: ./automate.sh deploytest
EOF
        ;;
esac